const TelegramApi = require("node-telegram-bot-api");
const token = "6668387908:AAHApdLiYvDeVY5gCZOAEf0R_JKKRp5161E";

const bot = new TelegramApi(token, { polling: true });

const fs = require("fs");

const admin = 6961357141;

const ua_local = require("./lang/ua.json");
const en_local = require("./lang/en.json");
const ru_local = require("./lang/ru.json");

const langOptions = {
  reply_markup: JSON.stringify({
    inline_keyboard: [
      [
        { text: "🇺🇦 UA", callback_data: "UA" },
        { text: "🇬🇧 EN", callback_data: "EN" },
        { text: "🇷🇺 RU", callback_data: "RU" },
      ],
    ],
  }),
};

const navOptions_ua = {
  reply_markup: JSON.stringify({
    inline_keyboard: [
      [{ text: `💸 ${ua_local.navPayText}`, callback_data: "/pay" }],
      [
        {
          text: `${ua_local.navGuideAllText}`,
          url: "https://www.youtube.com/watch?v=LyBUZ224jCc",
        },
      ],
      [{ text: `${ua_local.navTestText}`, callback_data: "sendFile" }],
      [
        {
          text: `${ua_local.navFirstUseText}`,
          url: "https://www.youtube.com/shorts/cjkF0I63mxw",
        },
      ],
      [{ text: `❔ ${ua_local.navFAQText}`, callback_data: "/faq" }],
      [{ text: `📞 ${ua_local.navConnectTrainer}`, url: "https://t.me/womang_global" }],
      [{ text: `🖥 ${ua_local.navWebSite}`, url: "https://womang.global/" }],
    ],
  }),
};

const navOptions_en = {
  reply_markup: JSON.stringify({
    inline_keyboard: [
      [{ text: `💸 ${en_local.navPayText}`, callback_data: "/pay" }],
      [
        {
          text: `${en_local.navGuideAllText}`,
          url: "https://www.youtube.com/watch?v=LyBUZ224jCc",
        },
      ],
      [{ text: `${en_local.navTestText}`, callback_data: "sendFile" }],
      [
        {
          text: `${en_local.navFirstUseText}`,
          url: "https://www.youtube.com/shorts/cjkF0I63mxw",
        },
      ],
      [{ text: `❔ ${en_local.navFAQText}`, callback_data: "/faq" }],
      [{ text: `📞 ${en_local.navConnectTrainer}`, url: "https://t.me/womang_global" }],
      [{ text: `🖥 ${en_local.navWebSite}`, url: "https://womang.global/" }],
    ],
  }),
};

const navOptions_ru = {
  reply_markup: JSON.stringify({
    inline_keyboard: [
      [{ text: `💸 ${ru_local.navPayText}`, callback_data: "/pay" }],
      [
        {
          text: `${ru_local.navGuideAllText}`,
          url: "https://www.youtube.com/watch?v=LyBUZ224jCc",
        },
      ],
      [{ text: `${ru_local.navTestText}`, callback_data: "sendFile" }],
      [
        {
          text: `${ru_local.navFirstUseText}`,
          url: "https://www.youtube.com/shorts/cjkF0I63mxw",
        },
      ],
      [{ text: `❔ ${ru_local.navFAQText}`, callback_data: "/faq" }],
      [{ text: `📞 ${ru_local.navConnectTrainer}`, url: "https://t.me/womang_global" }],
      [{ text: `🖥 ${ru_local.navWebSite}`, url: "https://womang.global/" }],
    ],
  }),
};

const backOptions_ua = {
  reply_markup: JSON.stringify({
    inline_keyboard: [
      [{ text: `↩️ ${ua_local.backText}`, callback_data: "/faq" }],
    ],
  }),
};
const backOptions_en = {
  reply_markup: JSON.stringify({
    inline_keyboard: [
      [{ text: `↩️ ${en_local.backText}`, callback_data: "/faq" }],
    ],
  }),
};
const backOptions_ru = {
  reply_markup: JSON.stringify({
    inline_keyboard: [
      [{ text: `↩️ ${ru_local.backText}`, callback_data: "/faq" }],
    ],
  }),
};

const paymentOptions_ua = {
  reply_markup: JSON.stringify({
    inline_keyboard: [
      [
        {
          text: `${ua_local.serviceTrain}`,
          url: "https://secure.wayforpay.com/payment/expander",
        },
      ],
      [
        {
          text: `${ua_local.serviceChimed}`,
          url: "https://secure.wayforpay.com/payment/oil",
        },
      ],
      [
        {
          text: `${ua_local.serviceMesabon}`,
          url: "https://secure.wayforpay.com/payment/onlinemonth",
        },
      ],
      [
        {
          text: `${ua_local.serviceReflexIndiv}`,
          url: "https://secure.wayforpay.com/payment/vipmonth",
        },
      ],
      [
        {
          text: `${ua_local.serviceReflexIndivOnce}`,
          url: "https://secure.wayforpay.com/payment/indiv",
        },
      ],
      [
        {
          text: `${ua_local.serviceReflexOnce}`,
          url: "https://secure.wayforpay.com/payment/reflex",
        },
      ],
      [{ text: `↩️ ${ua_local.backText}`, callback_data: "/menu" }],
    ],
  }),
};

const paymentOptions_en = {
  reply_markup: JSON.stringify({
    inline_keyboard: [
      [
        {
          text: `${en_local.serviceTrain}`,
          url: "https://secure.wayforpay.com/payment/expander",
        },
      ],
      [
        {
          text: `${en_local.serviceChimed}`,
          url: "https://secure.wayforpay.com/payment/oil",
        },
      ],
      [
        {
          text: `${en_local.serviceMesabon}`,
          url: "https://secure.wayforpay.com/payment/onlinemonth",
        },
      ],
      [
        {
          text: `${en_local.serviceReflexIndiv}`,
          url: "https://secure.wayforpay.com/payment/vipmonth",
        },
      ],
      [
        {
          text: `${en_local.serviceReflexIndivOnce}`,
          url: "https://secure.wayforpay.com/payment/indiv",
        },
      ],
      [
        {
          text: `${en_local.serviceReflexOnce}`,
          url: "https://secure.wayforpay.com/payment/reflex",
        },
      ],
      [{ text: `↩️ ${en_local.backText}`, callback_data: "/menu" }],
    ],
  }),
};

const paymentOptions_ru = {
  reply_markup: JSON.stringify({
    inline_keyboard: [
      [
        {
          text: `${ru_local.serviceTrain}`,
          url: "https://secure.wayforpay.com/payment/expander",
        },
      ],
      [
        {
          text: `${ru_local.serviceChimed}`,
          url: "https://secure.wayforpay.com/payment/oil",
        },
      ],
      [
        {
          text: `${ru_local.serviceMesabon}`,
          url: "https://secure.wayforpay.com/payment/onlinemonth",
        },
      ],
      [
        {
          text: `${ru_local.serviceReflexIndiv}`,
          url: "https://secure.wayforpay.com/payment/vipmonth",
        },
      ],
      [
        {
          text: `${ru_local.serviceReflexIndivOnce}`,
          url: "https://secure.wayforpay.com/payment/indiv",
        },
      ],
      [
        {
          text: `${ru_local.serviceReflexOnce}`,
          url: "https://secure.wayforpay.com/payment/reflex",
        },
      ],
      [{ text: `↩️ ${ru_local.backText}`, callback_data: "/menu" }],
    ],
  }),
};

const faqOptions_ua = {
  reply_markup: JSON.stringify({
    inline_keyboard: [
      [
        { text: "1", callback_data: "1" },
        { text: "2", callback_data: "2" },
        { text: "3", callback_data: "3" },
        { text: "4", callback_data: "4" },
        { text: "5", callback_data: "5" },
      ],
      [
        { text: "6", callback_data: "6" },
        { text: "7", callback_data: "7" },
        { text: "8", callback_data: "8" },
        { text: "9", callback_data: "9" },
        { text: "10", callback_data: "10" },
      ],
      [
        { text: "11", callback_data: "11" },
        { text: "12", callback_data: "12" },
        { text: "13", callback_data: "13" },
        { text: "14", callback_data: "14" },
        { text: "15", callback_data: "15" },
      ],
      [{ text: `↩️ ${ua_local.backText}`, callback_data: "/menu" }],
    ],
  }),
};

const faqOptions_en = {
  reply_markup: JSON.stringify({
    inline_keyboard: [
      [
        { text: "1", callback_data: "1" },
        { text: "2", callback_data: "2" },
        { text: "3", callback_data: "3" },
        { text: "4", callback_data: "4" },
        { text: "5", callback_data: "5" },
      ],
      [
        { text: "6", callback_data: "6" },
        { text: "7", callback_data: "7" },
        { text: "8", callback_data: "8" },
        { text: "9", callback_data: "9" },
        { text: "10", callback_data: "10" },
      ],
      [
        { text: "11", callback_data: "11" },
        { text: "12", callback_data: "12" },
        { text: "13", callback_data: "13" },
        { text: "14", callback_data: "14" },
        { text: "15", callback_data: "15" },
      ],
      [{ text: `↩️ ${en_local.backText}`, callback_data: "/menu" }],
    ],
  }),
};

const faqOptions_ru = {
  reply_markup: JSON.stringify({
    inline_keyboard: [
      [
        { text: "1", callback_data: "1" },
        { text: "2", callback_data: "2" },
        { text: "3", callback_data: "3" },
        { text: "4", callback_data: "4" },
        { text: "5", callback_data: "5" },
      ],
      [
        { text: "6", callback_data: "6" },
        { text: "7", callback_data: "7" },
        { text: "8", callback_data: "8" },
        { text: "9", callback_data: "9" },
        { text: "10", callback_data: "10" },
      ],
      [
        { text: "11", callback_data: "11" },
        { text: "12", callback_data: "12" },
        { text: "13", callback_data: "13" },
        { text: "14", callback_data: "14" },
        { text: "15", callback_data: "15" },
      ],
      [{ text: `↩️ ${ru_local.backText}`, callback_data: "/menu" }],
    ],
  }),
};

const langChangeHandler = (id, language) => {
  fs.readFile(
    "./users/users.json",
    "utf8",
    function readFileCallback(err, res) {
      if (err) {
        console.log(err);
      } else {
        users = JSON.parse(res);
        if (!users.table.some((item) => item.id === id)) {
          users.table.push({ id: id, lang: language });
          json = JSON.stringify(users);
          fs.writeFile("./users/users.json", json, "utf8", () => {});
        } else if (
          !(users.table.find((item) => item.id === id).lang === language)
        ) {
          const index = users.table.findIndex((item) => item.id === id);
          users.table[index].lang = language;
          json = JSON.stringify(users);
          fs.writeFile("./users/users.json", json, "utf8", () => {});
        }
      }
    }
  );
};

const start = () => {
  bot.setMyCommands([
    { command: "/start", description: "👋 Start" },
    { command: "/menu", description: "📃 Navigation menu" },
    { command: "/info", description: "ℹ️ Basic information" },
    { command: "/pay", description: "💸 Payment" },
    { command: "/faq", description: "❔ FAQ" },
    {
      command: "/send_notification",
      description: "🌐 Notification (restricted)",
    },
  ]);

  bot.on("message", async (msg) => {
    const text = msg.text;
    const chatId = msg.chat.id;

    if (text === "/start") {
      langChangeHandler(chatId, "UA");
      return bot.sendPhoto(chatId, "./assets/greeting.JPG", langOptions);
    }

    let currentUserLang = "";

    fs.readFile(
      "./users/users.json",
      "utf8",
      function readFileCallback(err, res) {
        if (err) {
          console.log(err);
          currentUserLang = "UA";
        } else {
          users = JSON.parse(res);

          if (users.table.find((item) => item.id === chatId) === undefined) {
            langChangeHandler(chatId, "UA");
          } else {
            currentUserLang = users.table.find(
              (item) => item.id === chatId
            ).lang;

            if (currentUserLang === "UA") {
              const parts = text.split(" ", 2);
              const command = parts[0];
              const notification = text.slice(command.length + 1);
              if (command === "/send_notification") {
                if (chatId === admin && notification != "") {
                  const ids = users.table.map((item) => item.id);
                  ids.map((item) => {
                    bot.sendMessage(item, notification);
                  });
                } else {
                  return bot.sendMessage(
                    chatId,
                    "У вас немає доступа або поле пусте"
                  );
                }
              }
              if (text === "/menu") {
                return bot.sendPhoto(
                  chatId,
                  './assets/menu_ua.jpg',
                  navOptions_ua
                );
              }
              if (text === "/info") {
                return bot.sendMessage(chatId, `${ua_local.infoText}`);
              }
              if (text === "/pay") {
                return bot.sendPhoto(
                  chatId,
                  './assets/pay_ua.jpg',
                  paymentOptions_ua
                );
              }
              if (text === "/faq") {
                return bot.sendMessage(
                  chatId,
                  `${ua_local.faqText} ${ua_local.questionsString}`,
                  faqOptions_ua
                );
              }
              // return bot.sendMessage(chatId, `${ua_local.errorText}`);
            } else if (currentUserLang === "EN") {
              const parts = text.split(" ", 2);
              const command = parts[0];
              const notification = text.slice(command.length + 1);
              if (command === "/send_notification") {
                if (chatId === admin && notification != "") {
                  const ids = users.table.map((item) => item.id);
                  ids.map((item) => {
                    bot.sendMessage(item, notification);
                  });
                } else {
                  return bot.sendMessage(
                    chatId,
                    "You`re not allowed to use this command or text field is empty"
                  );
                }
              }
              if (text === "/menu") {
                return bot.sendPhoto(
                  chatId,
                  './assets/menu_en.jpg',
                  navOptions_en
                );
              }
              if (text === "/info") {
                return bot.sendMessage(chatId, `${en_local.infoText}`);
              }
              if (text === "/pay") {
                return bot.sendPhoto(
                  chatId,
                  './assets/pay_en.jpg',
                  paymentOptions_en
                );
              }
              if (text === "/faq") {
                return bot.sendMessage(
                  chatId,
                  `${en_local.faqText} ${en_local.questionsString}`,
                  faqOptions_en
                );
              }
              // return bot.sendMessage(chatId, `${en_local.errorText}`);
            } else if (currentUserLang === "RU") {
              const parts = text.split(" ", 2);
              const command = parts[0];
              const notification = text.slice(command.length + 1);
              if (command === "/send_notification") {
                if (chatId === admin && notification != "") {
                  const ids = users.table.map((item) => item.id);
                  ids.map((item) => {
                    bot.sendMessage(item, notification);
                  });
                } else {
                  return bot.sendMessage(
                    chatId,
                    "У вас нет доступа или поле пустое"
                  );
                }
              }
              if (text === "/menu") {
                return bot.sendPhoto(
                  chatId,
                  './assets/menu_ru.jpg',
                  navOptions_ru
                );
              }
              if (text === "/info") {
                return bot.sendMessage(chatId, `${ru_local.infoText}`);
              }
              if (text === "/pay") {
                return bot.sendPhoto(
                  chatId,
                  './assets/pay_ru.jpg',
                  paymentOptions_ru
                );
              }
              if (text === "/faq") {
                return bot.sendMessage(
                  chatId,
                  `${ru_local.faqText} ${ru_local.questionsString}`,
                  faqOptions_ru
                );
              }
              // return bot.sendMessage(chatId, `${ru_local.errorText}`);
            }
          }
        }
      }
    );
  });

  bot.on("callback_query", async (msg) => {
    const action = msg.data;
    const chatId = msg.message.chat.id;

    if (action === "UA") {
      langChangeHandler(chatId, action);

      return bot.sendPhoto(
        chatId,
        './assets/menu_ua.jpg',
        navOptions_ua
      );
    } else if (action === "EN") {
      langChangeHandler(chatId, action);

      return bot.sendPhoto(
        chatId,
        './assets/menu_en.jpg',
        navOptions_en
      );
    } else if (action === "RU") {
      langChangeHandler(chatId, action);

      return bot.sendPhoto(
        chatId,
        './assets/menu_ru.jpg',
        navOptions_ru
      );
    }

    let currentUserLang = "";

    fs.readFile(
      "./users/users.json",
      "utf8",
      function readFileCallback(err, res) {
        if (err) {
          console.log(err);
          currentUserLang = "UA";
        } else {
          users = JSON.parse(res);
          currentUserLang = users.table.find((item) => item.id === chatId).lang;

          if (currentUserLang === "UA") {
            if (action === "sendFile") {
              return bot.sendDocument(
                chatId,
                "./assets/Самодіагностика МТД.pdf"
              );
            } else if (action === "/menu") {
              return bot.sendPhoto(
                chatId,
                './assets/menu_ua.jpg',
                navOptions_ua
              );
            } else if (action === "/pay") {
              return bot.sendPhoto(
                chatId,
                './assets/pay_ua.jpg',
                paymentOptions_ua
              );
            } else if (action === "/faq") {
              return bot.sendMessage(
                chatId,
                `${ua_local.faqText} ${ua_local.questionsString}`,
                faqOptions_ua
              );
            } else if (action != "UA" && action != "EN" && action != "RU") {
              const faqNumber = Number(action) - 1;
              return bot.sendMessage(
                chatId,
                ua_local.answers[faqNumber],
                backOptions_ua
              );
            }
          } else if (currentUserLang === "EN") {
            if (action === "sendFile") {
              return bot.sendDocument(
                chatId,
                "./assets/Self-Diagnosis of the pelvic floor muscles (PFM).pdf"
              );
            } else if (action === "/menu") {
              return bot.sendPhoto(
                chatId,
                './assets/menu_en.jpg',
                navOptions_en
              );
            } else if (action === "/pay") {
              return bot.sendPhoto(
                chatId,
                './assets/pay_en.jpg',
                paymentOptions_en
              );
            } else if (action === "/faq") {
              return bot.sendMessage(
                chatId,
                `${en_local.faqText} ${en_local.questionsString}`,
                faqOptions_en
              );
            } else if (action != "UA" && action != "EN" && action != "RU") {
              const faqNumber = Number(action) - 1;
              return bot.sendMessage(
                chatId,
                en_local.answers[faqNumber],
                backOptions_en
              );
            }
          } else if (currentUserLang === "RU") {
            if (action === "sendFile") {
              return bot.sendDocument(
                chatId,
                "./assets/Самодиагностика МТД.pdf"
              );
            } else if (action === "/menu") {
              return bot.sendPhoto(
                chatId,
                './assets/menu_ru.jpg',
                navOptions_ru
              );
            } else if (action === "/pay") {
              return bot.sendPhoto(
                chatId,
                './assets/pay_ru.jpg',
                paymentOptions_ru
              );
            } else if (action === "/faq") {
              return bot.sendMessage(
                chatId,
                `${ru_local.faqText} ${ru_local.questionsString}`,
                faqOptions_ru
              );
            } else if (action != "UA" && action != "EN" && action != "RU") {
              const faqNumber = Number(action) - 1;
              return bot.sendMessage(
                chatId,
                ru_local.answers[faqNumber],
                backOptions_ru
              );
            }
          }
        }
      }
    );
  });
};

start();
