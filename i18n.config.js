const { I18n } = require('i18n');
const path = require('path');

const i18n = new I18n({
  locales: ['uk', 'en'],
  defaultLocale: 'uk',
  directory: path.join('./', 'locales')
});

module.exports = i18n;